---
layout: post
title:  "Установка сканера Mustek 1248ub в Ubuntu 9.04 - 18.10"
date:   2009-09-16 18:20:14 -0300
categories: linux
---
![Сканер Mustek 1248ub](/media/img/se1248ub.jpg)

Вот такой вот сканер я себе купил года два назад. Но потом через год перешёл на Linux Ubuntu, где он у меня и перестал работать. Лениво погуглив и ничего не найдя, я поставил его пылиться на полку. И вот сегодня я его достал и таки настроил. А нужно было всего набрать в гугле "mustek 1248ub linux" :)

Настраивается он следующим образом:

открываем терминал и пишем
```
sudo mkdir -p  /usr/share/sane/gt68xx
cd /usr/share/sane/gt68xx
sudo wget http://www.meier-geinitz.de/sane/gt68xx-backend/firmware/SBSfw.usb
```

Всё! На этом установка завершается. Втыкаем сканер в usb и пользуемся на здоровье :)